# Compilation options
F90_PREFIX := 
F90 := $(F90_PREFIX) mpif90
F90STDFLAGS := -g
F90OPTFLAGS := -O2 -march=native -mtune=native 
F90REPORTFLAGS := 
F90FLAGS := $(F90STDFLAGS) $(F90OPTFLAGS) $(F90REPORTFLAGS)
FPPFLAGS := 
LDFLAGS := -llapack
J := -J
# Path to pFUnit (Unit testing Framework)
PFUNIT := $(ALL_CCCHOME)/opt/pfunit/pfunit-parallel
