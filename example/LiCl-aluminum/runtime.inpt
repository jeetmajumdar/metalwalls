# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps          1 # number of steps in the run
timestep      41.341 # timestep (a.u)
temperature   1500.0 # temperature

# Periodic boundary conditions
# ----------------------------
num_pbc       2

# Species definition
# ----------------------
species

  species_type
    name            Cl                 # name of the species
    count           500                # number of species
    charge point    -1.0               # permanent charge on the ions
    mass            35.453             # mass in amu

  species_type
    name            Li                 # name of the species 
    count           500                # number of species
    charge point    +1.0               # permanent charge on the ions
    mass            6.904              # mass in amu

  species_type                      
    name            Al1                # name of the species
    count           216                # number of species
    charge gaussian 0.955234657 0.0    # gaussian_width (bohr^-1) charge
    mass            26.982             # mass in amu
    mobile          False

  species_type
    name            Al2                # name of the species
    count           216                # number of species
    charge gaussian 0.955234657 0.0    # gaussian_width (bohr^-1) charge
    mass            26.982             # mass in amu
    mobile          False

# Electrode species definition
# ----------------------------
electrodes

  electrode_type
    name       Al1     # name of electrode
    species    Al1     # name of the species
    potential  +0.000  # fixed potential (a.u.)

  electrode_type
    name       Al2     # name of electrode
    species    Al2     # name of the species
    potential  +0.000  # fixed potential (a.u.)

  electrode_charges maze inversion 1.0e-10  # choice of algorithm for electrode charges
  charge_neutrality true                    # enforce charge neutrality

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol       1.6300594548881187e-5  # coulomb rtol 
    coulomb_rcut       22.95                  # coulomb cutoff (bohr)
    coulomb_ktol       1e-7                   # coulomb ktol

  fumi-tosi
     ft_rcut 22.95                            # ft cutoff (bohr)
     # ft parameters:
     ft_pair   Cl  Cl       1.59165237135E+00  1.15144639100E+02  1.44898634736E+02  1.00013802599E+02  1.68348394386E+00  1.64684809172E+00
     ft_pair   Cl  Li       1.91689383425E+00  5.47717660901E+01  9.85339942084E-04  8.36347039304E-04  1.49221171022E+00  9.24542774567E+00
     ft_pair   Cl  Al1      2.78305047173E+00  2.95419520674E+04  2.26109787141E+02  7.79562998574E+02  2.48465791920E+00  2.50096858136E+00
     ft_pair   Cl  Al2      2.78305047173E+00  2.95419520674E+04  2.26109787141E+02  7.79562998574E+02  2.48465791920E+00  2.50096858136E+00
     ft_pair   Li  Li       5.85430797535E+00  4.74550000000E+01  1.01010333216E-03  1.00690423058E-03  6.43758451936E+00  1.70598287505E+00
     ft_pair   Li  Al1      3.32441401252E+00  7.28749462530E+04  4.20076277069E-01  3.40747630564E+00  9.69127239560E+00  7.11626443067E+00
     ft_pair   Li  Al2      3.32441401252E+00  7.28749462530E+04  4.20076277069E-01  3.40747630564E+00  9.69127239560E+00  7.11626443067E+00

# Output section
# --------------
output
   default  1
   energies 1 Hamiltonian Kinetic Potential Coulomb van_der_Waals Coulomb_lr Coulomb_keq0 Coulomb_sr Coulomb_self Coulomb_molecule Fumi-Tosi Fumi-Tosi_molecule
