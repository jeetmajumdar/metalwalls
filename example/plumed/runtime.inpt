# System configuration
# ====================

# Global simulation parameters
# ----------------------------
num_steps       100 # number of steps in the run
timestep     41.341 # timestep (a.u)
temperature   298.0 # temperature (K)

# Periodic boundary conditions
# ----------------------------
num_pbc  2

# Thermostat
# ----------
thermostat
   chain_length    5
   relaxation_time 4134.1
   max_iteration   50
   tolerance       1.0e-17

# Plumed
# ------
plumed plumed.inpt  # PLUMED input file

# Species definition
# ---------------
species

  species_type
    name        O         # name of the species
    count      2159       # number of species
    charge  point -0.8476 # permanent charge on the ions
    mobile True
    mass   15.9994        # mass in amu                 

  species_type
    name H1               # name of the species
    count     2159        # number of species
    charge point 0.4238   # permanent charge on the ions
    mobile True                                         
    mass 1.008            # mass in amu                 
                                                         
  species_type                                          
    name H2               # name of the species
    count     2159        # number of species
    charge point 0.4238   # permanent charge on the ions
    mobile True                                         
    mass 1.008            # mass in amu                 

  species_type
   name          Na       # name of the species
   count         1        # number of species
   charge point  1.0      # permanent charge on the ions
   mobile True                                          
   mass          22.9898  # mass in amu                 

  species_type
    name C1                         # name of the species
    count 2400                      # number of species 
    mass 12.0                       # mass in amu
    mobile False
    charge gaussian 0.955234657 0.0 # gaussian_width (bohr^-1) charge

  species_type
    name C2                         # name of the species
    count 2400                      # number of species 
    mass 12.0                       # mass in amu
    mobile False
    charge gaussian 0.955234657 0.0 # gaussian_width (bohr^-1) charge


# Molecule definitions
# --------------------
molecules

  molecule_type
    name SPCE        # name of molecule
    count 2159       # number of molecules
    sites O H1 H2    # molecule's sites

    # Rigid constraints
    constraint  O H1 1.88973 # constrained radius for a pair of sites (bohr)
    constraint  O H2 1.88973 # constrained radius for a pair of sites (bohr)
    constraint H1 H2 3.08589 # constrained radius for a pair of sites (bohr)

    constraints_algorithm rattle 1.0e-7 100


# Electrode species definition
# ----------------------
electrodes

  electrode_type
    name       C1             # name of electrode
    species    C1             # name of the species
    potential  -0.0           # fixed potential

  electrode_type
    name       C2             # name of the species
    species    C2             # name of the species
    potential  +0.0           # fixed potential

  electrode_charges constant_charge

# Interactions definition
# -----------------------
interactions
  coulomb
    coulomb_rtol       2.0e-5        # coulomb rtol
    coulomb_rcut       32.2204127576 # coulomb cutoff (bohr)
    coulomb_ktol       1.0e-6        # coulomb ktol

  lennard-jones
    lj_rcut 32.2204127576            # lj cutoff (bohr)
    # lj parameters: epsilon in kJ/mol, sigma in angstrom
    lj_pair O  O  0.650200 3.166000
    lj_pair O  Na 0.521578 2.876500
    lj_pair O  C1 0.391723 3.190000
    lj_pair O  C2 0.391723 3.190000
    lj_pair Na C1 0.314233 2.900500
    lj_pair Na C2 0.314233 2.900500

# Output section
# --------------
output
  default 0
  step 10 
  lammps 100 
  restart 100 alternate  
  energies 100
  density 1000 100
