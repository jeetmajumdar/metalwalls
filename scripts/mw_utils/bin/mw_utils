#! /usr/bin/env python
###################################################################
#                mw.py                                            #
#  Created : Jan 2017                                             #
#                                                                 #
#  Author:                                                        #
#     Matthieu Haefele                                            #
#     matthieu.haefele@maisondelasimulation.fr                    #
#     Maison de la Simulation USR3441                             #
#                                                                 #
#  General utility tool that manages/assits in production duties  #
###################################################################

import os
import re
import sys
import argparse

import mw_utils as mwu

def status(args):
  """Print on stdout the job status"""
  print mwu.status(args.dir)

def restart(args):
  """Create a restart directory"""
  to_print = mwu.restart(args.dir_from, args.dir_to)
  if to_print != None:
    print to_print  

def equil(args):
  """Copy the right files in an existing directory"""
  to_print = mwu.equil(args.dir_from, args.dir_to)
  if to_print != None:
    print to_print  

def ls(args):
  """List available experiments"""
  print mwu.ls(args.dir_from, args.dir_to)


def build_parser():
  # create the top-level parser
  parser = argparse.ArgumentParser(prog='mw')
  #parser.add_argument('-o', type=str, default="metrics.json", help='name of the output file (default metrics.json)')
  subparsers = parser.add_subparsers(help='sub-command help')
 
  # create the parser for the "status" command
  parser_status = subparsers.add_parser('status', help='Get the status of a run')
  parser_status.add_argument('-d', '--dir', type=str, default=".", help='Directory of the considered run (default .)')
  parser_status.set_defaults(func=status)
 
  # create the parser for the "restart" command
  parser_restart = subparsers.add_parser('restart', help='Build a directory and copy the appropriate files to be able to launch metalwalls')
  parser_restart.add_argument('-f', '--dir_from', type=str, help='Directory of the run that should be continued (default runxxx with xxx the largest number in the current directory)')
  parser_restart.add_argument('-t', '--dir_to', type=str, help='Directory created for the restarted run (default xxx+1 if xxx found in dir_from directory name)')
  parser_restart.set_defaults(func=restart)
 
  # create the parser for the "equil" command
  parser_restart = subparsers.add_parser('equil', help='Use an existing directory and copy the appropriate files except the runtime.inpt to be able to launch metalwalls')
  parser_restart.add_argument('-f', '--dir_from', type=str, help='Directory of the run that should be continued (default runxxx with xxx the largest number in the current directory which contains a finished run)')
  parser_restart.add_argument('-t', '--dir_to', type=str, help='Directory used for the restarted run (default xxx+1 if xxx found in dir_from directory name)')
  parser_restart.set_defaults(func=equil)
 

  return parser



if __name__ == '__main__':
  p =  build_parser()
  args = p.parse_args(sys.argv[1:])
  args.func(args)

