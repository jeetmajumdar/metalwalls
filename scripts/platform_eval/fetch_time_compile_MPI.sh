
echo "MPI libs on 4 nodes   Time(s)"

for i in intel17.0.6.256 intel18.0.1.163 intel18.0.3.222
do
  echo $i `cat $i/run.out | grep "Total elapsed time:" | tr -s " " | cut -d ' ' -f 4`
done

