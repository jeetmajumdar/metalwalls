"""
Module defines a generic TestRun class

A test run is decomposed into 4 steps:

  1 - Setup
  2 - Run
  3 - Validate output
  4 - Clean up

"""
from validator import ValidatorError

class TestRunnerError(Exception):
    pass

class TestRunner:
    def __init__(self, runner, validators, testdir):
        """A Generic class to run tests

        Parameters:

        runner: Runner like object
            an object which implements the run method
        validators: sequence of Validator like objects
            a sequence of object which implements the validate method
        """
        self.runner = runner
        self.validators = validators
        self.testdir = testdir

    def setup(self):
        """Setup test environment"""
        pass

    def tearDown(self):
        """Tear down test environment"""
        pass

    def test(self):
        """Run the test"""
        self.setup()
        self.runner.run(cwd=self.testdir)
        failure = False
        for v in self.validators:
            try:
                v.validate(cwd=self.testdir)
            except ValidatorError as error:
                print(error)
                failure = True

        if failure:
            raise TestRunnerError

        self.tearDown()
