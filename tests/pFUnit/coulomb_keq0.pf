module Test_coulomb_keq0_mod

contains

   @test
   subroutine test_potential_melt_to_elec_AlElectrodes()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type
      use MW_ewald, only: MW_ewald_t, &
            MW_ewald_define_type => define_type, &
            MW_ewald_define_kpoints => define_kpoints, &
            MW_ewald_void_type => void_type, &
            MW_ewald_setup_cossin_ions => setup_cossin_ions, &
            MW_ewald_setup_cossin_elec => setup_cossin_elec
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      use MW_coulomb_keq0, only: MW_coulomb_keq0_qmelt2Qelec_potential => qmelt2Qelec_potential
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_ewald_local_work => setup_ewald_local_work, &
            MW_localwork_setup_pair_ion2atom_local_work => setup_pair_ion2atom_local_work, &
            MW_localwork_setup_atoms_local_work => setup_atoms_local_work
      implicit none
      ! Parameters
      ! ----------
      character(100), parameter :: test_data_in = "AlElectrodes_melt2elec.data"
      real(wp), parameter :: tolerance = 2.0e-14_wp

      ! Locals
      ! ------
      integer, parameter :: num_ion_types = 2
      integer, parameter :: num_elec_types = 2
      integer, parameter :: num_ions = 128
      integer, parameter :: num_atoms = 108
      integer, parameter :: num_pbc = 2

      ! Box
      type(MW_box_t) :: box !< simulation box
      real(wp), parameter :: a = 22.7474961445_wp
      real(wp), parameter :: b = 22.4394807143_wp
      real(wp), parameter :: c = 64.9010719657_wp

      ! Ewald
      type(MW_ewald_t) :: ewald
      real(wp), parameter :: rcut = 11.4_wp
      real(wp), parameter :: rtol = exp(-7.84_wp)/rcut ! chose to match alpha in metalwalls input
      real(wp), parameter :: ktol = 1.0e-7_wp
      real(wp), parameter :: eta = 9.55234657000000E-01_wp

      ! Ions
      type(MW_ion_t) :: ions(num_ion_types)
      real(wp) :: xyz_ions(num_ions,3)

      ! Electrode Atoms
      type(MW_electrode_t) :: electrodes(num_elec_types)
      real(wp) :: xyz_atoms(num_atoms,3)
      real(wp) :: q_atoms(num_atoms)
      real(wp) :: voronoi = 0.0_wp

      ! Local work
      integer :: comm_rank, comm_size
      type(MW_localwork_t) :: localwork
      real(wp), allocatable :: pot_elec_ref(:,:), pot_elec(:)
      ! Setup
      ! -----

      allocate(pot_elec_ref(num_atoms,4), pot_elec(num_atoms))
      pot_elec_ref(:,:) = 0.0_wp
      pot_elec(:) = 0.0_wp

      comm_rank = 0
      comm_size = 1
      call MW_box_define_type(box, a, b, c)
      call MW_ewald_define_type(ewald, rtol, rcut, ktol)
      call MW_ewald_define_kpoints(ewald, 2, box, num_ions, num_atoms)


      ! Ion #1
      call MW_ion_define_type(ions(1),"Cl",64, -1.0_wp,0.0_wp,0.0_wp,0.0_wp, &
                  0.0_wp, 35.453_wp, .true., .false.)
      ! Ion #2
      call MW_ion_define_type(ions(2), "Li", 64, +1.0_wp,0.0_wp,0.0_wp,0.0_wp, &
                  0.0_wp, 6.941_wp, .true., .false.)

      ions(1)%offset = 0
      ions(2)%offset = 64

      ! Electrode
      call MW_electrode_define_type(electrodes(1), "C1", 54, eta, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      call MW_electrode_define_type(electrodes(2), "C2", 54, eta, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)

      electrodes(1)%offset = 0
      electrodes(2)%offset = 54

      call MW_localwork_define_type(localwork)
      call MW_localwork_setup_ewald_local_work(localwork, ewald, num_pbc, box, comm_size, comm_rank)
      call MW_localwork_setup_pair_ion2atom_local_work(localwork, electrodes, ions, comm_size, comm_rank)
      call MW_localwork_setup_atoms_local_work(localwork, electrodes, comm_size, comm_rank)

      call test_load_data(test_data_in, &
            num_ions, num_atoms, &
            xyz_ions, xyz_atoms, q_atoms, &
            pot_elec_ref)

      call MW_ewald_setup_cossin_ions(ewald, box, xyz_ions)
      call MW_ewald_setup_cossin_elec(ewald, box, xyz_atoms)
      pot_elec(:) = 0.0_wp

      call MW_coulomb_keq0_qmelt2Qelec_potential(localwork, ewald, box, &
            ions, xyz_ions, electrodes, xyz_atoms, pot_elec)

      @assertEqual(pot_elec_ref(:,2), pot_elec(:), tolerance)

      deallocate(pot_elec_ref, pot_elec)
      call MW_localwork_void_type(localwork)
      call MW_ewald_void_type(ewald)
   end subroutine test_potential_melt_to_elec_AlElectrodes

   ! ========================================================================
   @test
   subroutine test_potential_elec_to_elec_AlElectrodes()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type
      use MW_ewald, only: MW_ewald_t, &
            MW_ewald_define_type => define_type, &
            MW_ewald_define_kpoints => define_kpoints, &
            MW_ewald_void_type => void_type, &
            MW_ewald_setup_cossin_ions => setup_cossin_ions, &
            MW_ewald_setup_cossin_elec => setup_cossin_elec
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      use MW_coulomb_keq0, only: MW_coulomb_keq0_Qelec2Qelec_potential => Qelec2Qelec_potential
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_ewald_local_work => setup_ewald_local_work, &
            MW_localwork_setup_pair_atom2atom_local_work => setup_pair_atom2atom_local_work, &
            MW_localwork_setup_atoms_local_work => setup_atoms_local_work
      implicit none
      ! Parameters
      ! ----------
      character(100), parameter :: test_data_in = "AlElectrodes_elec2elec.data"
      real(wp), parameter :: tolerance = 1.0e-14_wp

      ! Locals
      ! ------
      integer, parameter :: num_ion_types = 2
      integer, parameter :: num_elec_types = 2
      integer, parameter :: num_ions = 128
      integer, parameter :: num_atoms = 108
      integer, parameter :: num_pbc = 2

      ! Box
      type(MW_box_t) :: box !< simulation box
      real(wp), parameter :: a = 22.7474961445_wp
      real(wp), parameter :: b = 22.4394807143_wp
      real(wp), parameter :: c = 64.9010719657_wp

      ! Ewald
      type(MW_ewald_t) :: ewald
      real(wp), parameter :: rcut = 11.4_wp
      real(wp), parameter :: rtol = exp(-7.84_wp)/rcut ! chose to match alpha in metalwalls input
      real(wp), parameter :: ktol = 1.0e-7_wp

      ! Ions
      type(MW_ion_t) :: ions(num_ion_types)
      real(wp) :: xyz_ions(num_ions,3)

      ! Electrode Atoms
      real(wp), parameter :: eta = 9.55234657000000E-01_wp
      type(MW_electrode_t) :: electrodes(num_elec_types)
      real(wp) :: xyz_atoms(num_atoms,3)
      real(wp) :: q_atoms(num_atoms)
      real(wp) :: voronoi = 0.0_wp

      ! Local work
      integer :: comm_size
      integer :: comm_rank
      type(MW_localwork_t) :: localwork
      real(wp), allocatable :: pot_elec_ref(:,:), pot_elec(:)

      ! Setup
      ! -----
      allocate(pot_elec_ref(num_atoms,4), pot_elec(num_atoms))
      pot_elec_ref(:,:) = 0.0_wp
      pot_elec(:) = 0.0_wp
      comm_size = 1
      comm_rank = 0
      call MW_box_define_type(box, a, b, c)
      call MW_ewald_define_type(ewald, rtol, rcut, ktol)
      call MW_ewald_define_kpoints(ewald, 2, box, num_ions, num_atoms)

      ! Ion #1
      call MW_ion_define_type(ions(1),"Cl",64, -1.0_wp,0.0_wp,0.0_wp,0.0_wp, &
                  0.0_wp, 35.453_wp, .true., .false.)
      ! Ion #2
      call MW_ion_define_type(ions(2), "Li", 64, +1.0_wp,0.0_wp,0.0_wp,0.0_wp, &
                  0.0_wp, 6.941_wp, .true., .false.)

      ions(1)%offset = 0
      ions(2)%offset = 64

      ! Electrode
      call MW_electrode_define_type(electrodes(1), "C1", 54, eta, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      call MW_electrode_define_type(electrodes(2), "C2", 54, eta, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)

      electrodes(1)%offset = 0
      electrodes(2)%offset = 54

      call MW_localwork_define_type(localwork)
      call MW_localwork_setup_ewald_local_work(localwork, ewald, num_pbc, box, comm_size, comm_rank)
      call MW_localwork_setup_pair_atom2atom_local_work(localwork, electrodes, comm_size, comm_rank)
      call MW_localwork_setup_atoms_local_work(localwork, electrodes, comm_size, comm_rank)

      call test_load_data(test_data_in, &
            num_ions, num_atoms, &
            xyz_ions, xyz_atoms, q_atoms, &
            pot_elec_ref)

      call MW_ewald_setup_cossin_ions(ewald, box, xyz_ions)
      call MW_ewald_setup_cossin_elec(ewald, box, xyz_atoms)
      pot_elec(:) = 0.0_wp

      call MW_coulomb_keq0_Qelec2Qelec_potential(localwork, ewald, box, &
            electrodes, xyz_atoms, q_atoms, pot_elec)

      @assertEqual(pot_elec_ref(:,2), pot_elec(:), tolerance)

      deallocate(pot_elec_ref, pot_elec)
      call MW_localwork_void_type(localwork)
      call MW_ewald_void_type(ewald)
   end subroutine test_potential_elec_to_elec_AlElectrodes

   ! ========================================================================
   @test
   subroutine test_forces_melt_AlElectrodes()
      use pfunit_mod
      use MW_kinds, only: wp
      use MW_box, only: MW_box_t, &
            MW_box_define_type => define_type
      use MW_ewald, only: MW_ewald_t, &
            MW_ewald_define_type => define_type, &
            MW_ewald_define_kpoints => define_kpoints, &
            MW_ewald_void_type => void_type, &
            MW_ewald_setup_cossin_ions => setup_cossin_ions, &
            MW_ewald_setup_cossin_elec => setup_cossin_elec
      use MW_ion, only: MW_ion_t, &
            MW_ion_define_type => define_type
      use MW_electrode, only: MW_electrode_t, &
            MW_electrode_define_type => define_type
      use MW_coulomb_keq0, only: MW_coulomb_keq0_melt_forces => melt_forces
      use MW_localwork, only: MW_localwork_t, &
            MW_localwork_define_type => define_type, &
            MW_localwork_void_type => void_type, &
            MW_localwork_setup_ewald_local_work => setup_ewald_local_work, &
            MW_localwork_setup_pair_ion2ion_local_work => setup_pair_ion2ion_local_work, &
            MW_localwork_setup_pair_atom2ion_local_work => setup_pair_atom2ion_local_work
      implicit none
      ! Parameters
      ! ----------
      character(100), parameter :: test_data_in = "AlElectrodes_melt_forces.data"
      real(wp), parameter :: tolerance = 1.0e-14_wp

      ! Locals
      ! ------
      integer, parameter :: num_ion_types = 2
      integer, parameter :: num_elec_types = 2
      integer, parameter :: num_ions = 128
      integer, parameter :: num_atoms = 108
      integer, parameter :: num_pbc = 2

      ! Box
      type(MW_box_t) :: box !< simulation box
      real(wp), parameter :: a = 22.7474961445_wp
      real(wp), parameter :: b = 22.4394807143_wp
      real(wp), parameter :: c = 64.9010719657_wp

      ! Ewald
      type(MW_ewald_t) :: ewald
      real(wp), parameter :: rcut = 11.2_wp
      real(wp), parameter :: rtol = 1.0e-15_wp
      real(wp), parameter :: ktol = 1.0e-10_wp
      real(wp), parameter :: eta = 9.55234657000000E-01_wp

      ! Ions
      type(MW_ion_t) :: ions(num_ion_types)
      real(wp) :: xyz_ions(num_ions,3)

      ! Electrode Atoms
      type(MW_electrode_t) :: electrodes(num_elec_types)
      real(wp) :: xyz_atoms(num_atoms,3)
      real(wp) :: q_atoms(num_atoms)
      real(wp) :: voronoi = 0.0_wp

      ! Localwork
      integer :: comm_size, comm_rank
      type(MW_localwork_t) :: localwork

      real(wp), allocatable :: forces_ref(:,:,:)
      real(wp), allocatable :: forces(:,:)
      ! Setup
      ! -----

      comm_size = 1
      comm_rank = 0
      call MW_box_define_type(box, a, b, c)
      call MW_ewald_define_type(ewald, rtol, rcut, ktol)
      call MW_ewald_define_kpoints(ewald, 2, box, num_ions, num_atoms)

      ! Ion #1
      call MW_ion_define_type(ions(1),"Cl",64, -1.0_wp,0.0_wp,0.0_wp,0.0_wp, &
                  0.0_wp, 35.453_wp, .true., .false.)
      ions(1)%offset = 0
      ! Ion #2
      call MW_ion_define_type(ions(2), "Li", 64, +1.0_wp,0.0_wp,0.0_wp,0.0_wp, &
                  0.0_wp, 6.941_wp, .true., .false.)
      ions(2)%offset = 64

      ! Electrode
      call MW_electrode_define_type(electrodes(1), "C1", 54, eta, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)
      call MW_electrode_define_type(electrodes(2), "C2", 54, eta, 0.0_wp, 0.0_wp, .false., 0.0_wp, 0.0_wp, 0.0_wp, voronoi)

      electrodes(1)%offset = 0
      electrodes(2)%offset = 54

      call MW_localwork_define_type(localwork)
      call MW_localwork_setup_ewald_local_work(localwork, ewald, num_pbc, box, comm_size, comm_rank)
      call MW_localwork_setup_pair_ion2ion_local_work(localwork, ions, comm_size, comm_rank)
      call MW_localwork_setup_pair_atom2ion_local_work(localwork, ions, electrodes, comm_size, comm_rank)

      allocate(forces_ref(num_ions,3,3))
      call test_load_forces_data(test_data_in, &
            num_ions, num_atoms, &
            xyz_ions, xyz_atoms, q_atoms, &
            forces_ref)

      call MW_ewald_setup_cossin_ions(ewald, box, xyz_ions)
      call MW_ewald_setup_cossin_elec(ewald, box, xyz_atoms)

      allocate(forces(num_ions,3))
      forces(:,:) = 0.0_wp

      call MW_coulomb_keq0_melt_forces(localwork, ewald, box, &
            ions, xyz_ions, electrodes, xyz_atoms, q_atoms, forces, .false.)
      @assertEqual(forces_ref(:,:,2), forces(:,:), tolerance)

      deallocate(forces)
      deallocate(forces_ref)
      call MW_localwork_void_type(localwork)
      call MW_ewald_void_type(ewald)
   end subroutine test_forces_melt_AlElectrodes

   ! ========================================================================
   subroutine test_load_data(test_data, &
         num_ions, num_atoms, &
         xyz_ions, xyz_atoms, q_atoms, pot_elec_ref)
      use MW_kinds, only: wp
      implicit none

      ! Parameters
      ! ----------
      character(*), intent(in) :: test_data
      integer, intent(in) :: num_ions !< number of melt particles
      integer, intent(in) :: num_atoms !< number of electrode atoms

      real(wp), intent(inout) :: xyz_ions(:,:)
      real(wp), intent(inout) :: xyz_atoms(:,:)
      real(wp), intent(inout) :: q_atoms(:)
      real(wp), intent(inout) :: pot_elec_ref(:,:)

      ! Local
      ! -----
      integer :: funit
      integer :: i, ixyz

      open(newunit=funit, file=test_data, form='formatted', status='old')

      do i = 1, num_ions
         read(funit, *) (xyz_ions(i,ixyz),ixyz=1,3)
      end do

      do i = 1, num_atoms
         read(funit, *) (xyz_atoms(i,ixyz), ixyz=1,3), q_atoms(i)
      end do

      do i = 1, num_atoms
         read(funit, *) pot_elec_ref(i,1), pot_elec_ref(i,2), pot_elec_ref(i,3), pot_elec_ref(i,4)
      end do

      close(funit)

   end subroutine test_load_data

   ! ========================================================================
   subroutine test_load_forces_data(test_data, &
         num_ions, num_atoms, &
         xyz_ions, xyz_atoms, q_atoms, forces_ref)
      use MW_kinds, only: wp
      implicit none

      ! Parameters
      ! ----------
      character(*), intent(in) :: test_data
      integer, intent(in) :: num_ions !< number of melt particles
      integer, intent(in) :: num_atoms !< number of electrode atoms

      real(wp), intent(inout) :: xyz_ions(:,:)
      real(wp), intent(inout) :: xyz_atoms(:,:)
      real(wp), intent(inout) :: q_atoms(:)
      real(wp), intent(inout) :: forces_ref(:,:,:)

      ! Local
      ! -----
      integer :: funit
      integer :: i, ixyz

      open(newunit=funit, file=test_data, form='formatted', status='old')

      do i = 1, num_ions
         read(funit, *) (xyz_ions(i,ixyz), ixyz=1,3)
      end do

      do i = 1, num_atoms
         read(funit, *) (xyz_atoms(i,ixyz), ixyz=1,3), q_atoms(i)
      end do

      do i = 1, num_ions
         read(funit, *) (forces_ref(i,ixyz,1), ixyz=1,3), &
               (forces_ref(i,ixyz,2), ixyz=1,3), &
               (forces_ref(i,ixyz,3), ixyz=1,3)
      end do

      close(funit)

   end subroutine test_load_forces_data

end module Test_coulomb_keq0_mod
