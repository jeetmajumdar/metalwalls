import unittest
import os.path
import glob
import numpy as np
import sys

import mwrun

class test_steele(unittest.TestCase):

  @classmethod
  def setUpClass(self):
    self.test_path = "steele"
    self.mw_exec = os.path.abspath(mwrun.glob_mw_exec)

  def run_conf(self, dirname, nranks):
    path_to_config = os.path.join(self.test_path, dirname)
    n = mwrun.mwrun(self.mw_exec, path_to_config)
    self.workdirs = [n.workdir]
    n.run_mw(nranks)

    ok, msg = n.compare_datafiles("forces.out", "forces.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("energies_breakdown.out", "energies_breakdown.ref")
    self.assertTrue(ok, msg)

    ok, msg = n.compare_datafiles("electrode_forces.out","electrode_forces.ref")
    self.assertTrue(ok, msg)

  def tearDown(self):
    for w in self.workdirs:  
      for f in glob.glob(os.path.join(w, "*.out")):
        os.remove(f)

  def test_steele_serial(self):
    self.run_conf("./", 1)

  def test_steele_parallel(self):
    self.run_conf("./", 4)
