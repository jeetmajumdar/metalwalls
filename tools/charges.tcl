# Usage: vmd -e charges.tcl trajectories.lammpstrj

set framecount [molinfo top get numframes]
set sel [atomselect top all]
for {set frame_no 0} {$frame_no<$framecount} {incr frame_no} {
$sel frame $frame_no
$sel set {user user2 user3} [$sel get {vx vy vz}]
}
$sel delete
